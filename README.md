# ![git](/img/docker-logo.svg) <img src="img/bigshop-logo.svg" align="right">

# Conceitos

- `Imagem:` Uma imagem no docker é basicamente como uma "classe" dentro da programação. Nela temos as instruções das rotinas para a criação de um container (Isso inclui variáveis de ambiente, portas de rede, volumes). Toda imagem possui uma outra imagem como base, por exemplo: Podemos utilizar a imagem ubuntu como base e instalar o nginx. Ao gerar um container a partir dessa imagem personalizada, temos um nginx rodando.

- `Container:` O container é a instância de uma imagem, ou seja, um "objeto". Ele é executado de forma isolada no host.

### Arquivos

- `Dockerfile:` Utilizando o nosso exemplo o Dockerfile é o arquivo da "classe". Podemos também comparar com uma receita de bolo. É dentro dele que definimos todos os passos e comportamentos do container. Por exemplo, queremos rodar um projeto php dentro de um sistema operacional debian. Utilizamos a imagem do debian como base, mas precisamos instalar o php na última versão. É dentro do Dockerfile que vamos escrever os passos de instalação do php e de suas extenções (apt install php8.1, etc...)

- `docker-compose.yml:` Diferente do Dockerfile, o docker-compose é utilizado quando precisamos gerenciar vários containers ao mesmo tempo. Com ele conseguimos definir os serviços (containers) que compõe a aplicação, rodar todos eles em um ambiente isolado, realizar configurações de rede entre os containers, definir o uso de volumes entre outras coisas.

# Comandos

## Manipulando containers

- `docker ps -a:`  Lista containers 
- `docker ps:`  Lista todos os containers em execução
- `docker stop <nome ou id do container>:`  Para o container
- `docker restart <nome ou id do container:>`  Reinicia o container
- `docker start <nome ou id do container>:`  Inicia o container
- `docker rm <nome ou id do container>:`  Deleta o container
- `docker stats <nome ou id do container>:`  Verifica o quanto de recurso o container está consumido
- `docker top <nome ou id do container>:`  verifica os processos que estão sendo executados no container
- `docker logs <nome ou id do container>:`  Exibe os logs do container
- `docker run -d <imagem>:`  Executa um container em segundo plano
- `docker run --name <nome_da-imagem> <imagem>:`  Define um nome para o container
- `docker container prune:`  Remove todos os containers que não estáo rodando

## Manipulando imagens
- `docker images:`  Lista todas as imagens
- `docker rmi <nome ou id>:`  remove uma imagem
- `docker commit <nome ou id> <nome_da_imagem>:` Gera uma imagem a partir de um container

## Executar comandos dentro do container
- `docker exec -it <nome ou id do container> /bin/bash:` acessa terminal do container

## Volumes
- `docker volume ls:` lista todos os volumes
- `docker volume rm <nome volume>:` remove um ou mais volumes
- `docker volume inspect <nome volume>:` exibe informações volume
- `docker volume create <nome volume>:` cria um volume
- `docker volume prune:` remove todos os volumes não usados

## Redes
- `docker network ls:` Lista todos as redes do docker
- `docker network create --driver <driver> <nome>:` Cria uma rede
- `docker run -d --name <nome_da_imagem> --network <nome> <imagem>:`  Adiciona um container a uma rede
- `docker network inspect <nome/id_da_rede>:` Exibe informações de uma rede
- `docker network prune:` Remove todas as redes não utilizadas
- `docker network rm <nome/id_da_rede>:` remove uma rede


## docker-compose
- `docker-compose up`: cria e inicia os contêineres
- `docker-compose build`: realiza apenas a etapa de build das imagens que serão utilizadas
- `docker-compose logs`: visualiza os logs dos contêineres
- `docker-compose restart:` reinicia os contêineres
- `docker-compose ps:` lista os contêineres
- `docker-compose scale:` permite aumentar o número de réplicas de um contêiner
- `docker-compose start:` inicia os contêineres
- `docker-compose stop:` paralisa os contêineres
- `docker-compose down:` paralisa e remove todos os contêineres e seus componentes como rede, imagem e volume.

<br>

### Para se aprofundar 
- <a href="https://www.jlcp.com.br/primeiros-passos-com-docker-conceitos/"> Primeiros passos com docker (Conceitos) </a>
- <a href="https://stack.desenvolvedor.expert/appendix/docker/"> Avançando no docker </a>

### Sobre Dockerfile e docker-compose
- <a href="https://blog.rocketseat.com.br/dockerfile-principais-comandos-para-criar-a-receita-da-imagem/">Dockerfile</a>
- <a href="https://blog.4linux.com.br/docker-compose-explicado/">docker-compose</a>
<br>
<br>
<p>Feito com muito ☕ & ❤️ para nós 👨🏻‍💻</p>
